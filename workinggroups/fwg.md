---
title: "KDE e.V. Financial Working Group"
layout: page
---

### Goals

The Financial Working Group supports the treasurer & the community in maintaining the financial health of the organization and implementing its financial goals.

The Financial Working Group has the following tasks:

+ Help write the budget plan.
* Help implement the budget plan: bring goals to the community, remind relevant parties of goals, communicate available budget to people.
* Know vital financial information of the organization.
* Provide financial information on request where appropriate and needed (e.g. annual report).

### Rules

The group has fewer than 9 members, and at least two.

The members are selected by the KDE e.V. for a two-year period.
An online vote is used to elect those members.
Members must sign an NDA regarding the internal financial
information that they handle.

The KDE e.V. board member who vacates the position of treasurer at the end of
a regular duty cycle on the board automatically joins the Financial Working Group
membership for a two-year period.

The Financial Working Group members **cannot** be selected as auditors of accounting.

Members can step down from the Financial Working Group at any time.
At such time, an online vote for new members may be arranged.

### Members

> Current members and the date of their election

+ David Edmundson david@davidedmundson.co.uk (2019-05-19)
+ Neofytos Kolokotronis neofytosk@posteo.net (2019-05-19)

#### Past Members

> Past members from 2019 onwards. A current member who
> has served previously may **also** be listed here.

+ Daniele E. Domenichelli ddomenichelli@drdanz.it (2019-05-19..2021-04-14)

### Contact

[kde-ev-financial-wg@kde.org](mailto:kde-ev-financial-wg@kde.org)

---
title: "KDE e.V. Working Groups"
layout: page
---

> The KDE e.V. Working groups are self-organizing, pro-active groups of
> KDE community members that operate in a self-proclaimed work-area.
> The Working Groups (WG) enable KDE e.V. to be a more effective support
> organisation for KDE by taking work off the board's shoulders and providing special expertise.

## Active Working Groups

- [Advisory Board Working Group](/workinggroups/abwg/)
- [Community Working Group (CWG)](/workinggroups/cwg/)
- [Financial Working Group (FiWG)](/workinggroups/fwg/)
- [KDE Free Qt Working Group](/workinggroups/kfqwg/)
- [Fundraising Working Group (FuWG)](/workinggroups/fundraisingwg/)
- [Onboarding Working Group](/workinggroups/onboarding/)
- [System Administration (sysadmin)](/workinggroups/sysadmin/)


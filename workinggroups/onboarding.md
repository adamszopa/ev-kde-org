---
title: "KDE e.V. Onboarding Working Group"
layout: page
---

### Goals

> What is the WG for.

### Rules

> Brief summary if there are rules and regulations; if there are
> lots of rules, refer to a charter and add it at the end.

### Members

> If the WG has members, list them here.

### Contact

You can contact the Onboarding Working Group under 
*whatever email is used*.

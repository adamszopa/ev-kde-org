---
title: "KDE e.V. KDE Free Qt Working Group"
layout: page
---

### Goals

To advise and assist KDE's elected board members in the KDE Free Qt Foundation.

### Rules

This working group contains the KDE e.V. representatives to the 
KDE Free Qt Foundation and additional members elected by the KDE e.V. 
membership. See the [charter](#charter), below.

### Members

+ Albert Astals Cid
+ Chris Rizzitello
+ David Edmundson
+ Eike Hein
+ Frederik Gladhorn
+ Martin Konold [\*]
+ Olaf Schmidt-Wischhöfer [\*]

[\*] These members are the KDE elected board members in the KDE Free Qt Foundation.

### Contact

You can contact the KDE Free Qt Working Group under
[kde-ev-free-qt-wg@kde.org](mailto:kde-ev-free-qt-wg@kde.org).

## Charter

### Transparency and confidentiality

Everything discussed in this Working Group is private to the Working Group members. It's the responsibility of the KDE elected representatives to do any public communication be it with the rest of the KDE Free Qt Foundation or with the rest of the public. The KDE elected representatives may delegate on other members of the Working Group some of those communication duties.

### Conflicts of interest

Working Group members should state their professional interests when joining the Working Group or if those interests change once they have joined the group. They are allowed to use this background when offering advise.

### Decision making

KDE's two elected representatives within the board of the Foundation are accountable to the KDE e.V. membership and
bound to the statutes of the foundation. They will listen to the advice of members of the Working Group, but the final responsibility for our actions would stay with them.

### How are members elected?

All members of the Free Qt Working Group can suggest changes to the list of members. The decision is made by the KDE e.V. board and by the elected KDE representatives in the Foundation.


---
title: Staff and Contractors
layout: page
menu_active: Organization
staffcontractors:
  - name: Adam Szopa
    title: Project Coordinator (contractor)
    email: adam.szopa<span>@</span>kde.org
    description: Adam started working for KDE in 2020. Adam mainly helps coordinate work related to the KDE Goals, Akademy and other parts of the Community. He has a masters degree in computer science and likes to relax by playing video games. He lives in Poland.    
    image: /corporate/pictures/adam.jpg
  - name: Allyson Alexandrou
    title: Event and Fundraising Coordinator (contractor)
    email: allyson.alexandrou<span>@</span>kde.org
    description: Allyson Alexandrou is a freelance Event Coordinator and started working for KDE in 2020. Allyson helps organize Akademy, KDE's yearly event that brings together the whole Community. Her background is in event planning, strategy, and management consulting. She has planned numerous large and small-scale events in corporate and university settings. She has a passion for travel and volunteering which has brought her around the world, helping local communities along the way. Allyson holds a Bachelor’s degree in Business Communication from Arizona State University and a Master’s degree in Global Management from the Thunderbird School of Global Management. In her spare time, she enjoys reading, creating music, and spending time at the beach.
    image: /corporate/pictures/allyson.jpg
  - name: Aniqa Khokhar
    title: Marketing Consultant (contractor)
    email: aniqa.khokhar<span>@</span>kde.org
    description: Aniqa Khokhar is a Marketing Consultant and started working for KDE e.V. in 2020. She contributes to the KDE Promo team activities and strengthens marketing efforts for both the Community and the organization. She has experience in managing marketing campaigns, social media, events, customers, business planning, corporate communications, and market research in education and non-profit sectors. In her spare time, she loves cooking, gardening, and travelling.
    image: /corporate/pictures/aniqa.jpg
  - name: Paul Brown
    title: Marketing Consultant (contractor)
    email: paul.brown<span>@</span>kde.org
    description: Paul Brown started working as Marketing Consultant for KDE e.V. in 2017. He comes from the world of publishing and has been a Free Software advocate since 1996. He works with the members of the Promo team setting goals, managing campaigns and analysing results. He also helps KDE projects optimise their communication strategies and copywrites and proofreads their websites and blog posts. In his spare time, he enjoys 3D printing, writing tutorials on Free Software usage and articles on writing and communication. He also "enjoys" watching TV series and movies and then ranting about how the creators are "lazy writers" on Reddit and Twitter.
    image: /corporate/pictures/paul.png
  - name: Petra Gillert
    title: Assistant to the Board
    email: petra<span>@</span>kde.org
    description: Petra Gillert is the assistant of KDE e.V.'s board since 2015. She supports the board and the organisation in all matters of organisation and finance and manages the office in Berlin. She ensures continuity in the organisation. In her spare time she enjoys bird watching.
    image: /corporate/pictures/petra.png
---

KDE e.V. supports the KDE Community in various ways, one of them is through its staff and contractors to work on key areas.

## Current staff and contractors

{% for people in page.staffcontractors %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="{{ people.image }}" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.name }}</h3>
    <p class="people-title">{{ people.title }}</p>
    <p class="people-email">{{ people.email }}</p>
    <p>{{ people.description }}</p>
  </div>
</div>
{% endfor %}

## Open positions

We are currently looking for:
* [a documentation writer](/2021/03/24/job-documentation/)
* [a project lead/event manager](/resources/jobad-projectlead2021.pdf)
* [a project/community manager](/resources/jobad-projectcommunitymanager2021.pdf)

You are welcome to contact kde-ev-board<span>@</span>kde.org with questions.

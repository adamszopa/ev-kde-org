---
title: 'KDE e.V. and KDE España Sign an Agreement Officially Recognizing KDE España As Oficial Representative of KDE e.V. in Spain'
date: 2010-07-14 00:00:00 
layout: post
---

<p>During Akademy 2010, held in Tampere (Finland), KDE e.V. and KDE Espa&#241;a signed an agreement making KDE Espa&#241;a the official representative of KDE e.V. in its territory.</p>

<p>The agreement highlights several of the key goals that KDE e.V. and KDE España share, that is, thepromotion of Free Software and KDE Software in particular, acting as an enabler for the community and the non profit nature of the organization.</p>

<p>Celeste Lyn Paul, KDE e.V. Board member, states: "It is great to see such a strong local KDE community grow to the point where it has professional, institutionalized representation. We're happy to work together and strengthen the KDE community both in Spain and Worldwide".</p>

<p>Albert Astals Cid, President of KDE España Board, said: "We're proud that we now have a formalized relationship with our international sister organisation. This will help us be more effective in supporting the Spanish KDE community and strengthen the bond between us and the worldwide KDE community".</p>

<p>This means KDE España will be the nearest contact for all organizations and individuals who want to contact KDE representatives regarding any issue related to KDE, including but not limited to claims about KDE copyrights infringements in the Spanish territory.</p>

<p>In exchange, KDE España has to report at least once a year about its activities (events, collaborations, agreements...) and its members to KDE e.V. to make sure the organization is acting in accordance with the guidelines set by KDE e.V.</p>

<p>Both organizations expect this agreement will help the KDE sister organisation in Spain to improve its visibility users, companies and local institutions and help make it more effective in what it does, while at the same time increasing the bond between the local Spanish KDE community and the international team.</p>


<h2>About KDE e.V.</h2>
<p>KDE e.V. is a registered non-profit organization that represents the KDE Project in legal and financial matters.</p>

<p>The Association aids in creating and distributing KDE by securing cash, hardware, and other donations, then using donations to aid KDE development and promotion.</p>

<p>The Association was originally founded to create a legal entity capable of representing the KDE Project in the KDE Free Qt Foundation. The growing popularity of KDE also made it necessary to establish a thin organizational layer that is capable of handling all legal and financial interests of the KDE developers. In 1997 KDE e.V. was registered as an association under German law.</p>


<h2>About KDE España</h2>

<p>KDE España is formed by developers and contributors of the KDE community with the aim of stimulating the development and use of the KDE software in the Spanish territory. Members of KDE España include a wide variety of KDE contributors, amongst them programmers, packagers,  documenters, translators and artists. KDE España was registered as an association under the Spanish law in 2009.</p>

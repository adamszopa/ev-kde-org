---
title: 'KDE e.V. Quarterly Report 2007Q1'
date: 2007-06-22 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2007Q1.pdf">KDE e.V.
Quarterly Report</a> is now available for January to March 2007. As usual it includes
reports of the board and the working groups about the KDE e.V. activities of the last
quarter and future plans.

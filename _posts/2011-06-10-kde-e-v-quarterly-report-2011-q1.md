---
title: 'KDE e.V. Quarterly Report 2011 Q1'
date: 2011-06-10 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2011_Q1.pdf">first quarterly report of 2011</a>.

This report covers January through March 2011, including statements from the board and the working groups of KDE e.V. about their activities, reports from sprints, financial information. A special report about conf.kde.in is included, and the KDE Free Qt Foundation makes a statement.
      

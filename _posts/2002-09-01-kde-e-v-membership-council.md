---
title: 'KDE e.V. Membership Council'
date: 2002-09-01 00:00:00 
layout: post
---

<a href="/corporate/board/">a list of the new board members</a>), statute changes were discussed (but, according to German associative law, could not be voted on because the intended changes have to be announced in the invitation), and (of course) any available computers were used to hack on KDE.

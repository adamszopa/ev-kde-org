---
title: "General Assembly KDE e.V. 2020"
layout: page
---

# General Assembly KDE e.V. 2020

The General Assembly took place monday 7 September 2020 at 13:00 CEST.

The meeting was held in a password protected online video conference with integrated chat, using an open source tool called [BigBlueButton](https://bigbluebutton.org/). The agenda and presentations were presented and shared using an open source tool called [OpenSlides](https://openslides.com/de). OpenSlides was also used to register the presence of e.V. members and record the elections. The list of participants was exported from OpenSlides for documentation purposes. 94 members were present, among them 2 non-voting associate members. 3 absentee members nominated representatives on their behalf. No non-member was present.

Both systems (BigBlueButton and OpenSlides) ran on KDE servers during the meeting. All members received access to these tools in advance of the meeting.

## Agenda

1. Greetings
2. Election of the meeting chairpersons
3. Status reports
   1. Report on activities
   2. Report from the treasurers
   3. Report from the auditors of the accounts
   4. Relieving the board
4. Report from representatives and working groups
   1. Report from the representative of the KDE Free Qt Foundation
   2. Report from the System Administration Working Group
   3. Report from the Community Working Group
   4. Report from the Financial Working Group
   5. Report from the Advisory Board Working Group
   6. Report from the Fundraising Working Group
5. Election of the board
6. Election of the auditors of accounting
7. Election of representatives to the KDE Free Qt Foundation
8. Miscellaneous discussion

## Minutes

### Welcome and election of chairpersons for the meeting

At 13:00 the meeting was opened by the president of the board, Aleix Pol. He thanked the organisers of the General Assembly.

Frederik Gladhorn was elected as chairperson for the meeting by general acclaim.

The chairperson noted that invitations to the General Assembly were sent out in the proper fashion and in a timely manner. There were no objections.

The agenda for the meeting was acknowledged by general acclaim.

The chairperson appointed Cornelius Schumacher as record keeper of the minutes.

The chairperson noted that the attendance of the meeting constituted a quorum of the general membership.

### Status reports

The status reports covered the activities of the e.V. since the last General Assembly. The report had been presented already on 05-09-2020 at the [Akademy](https://akademy.kde.org/2020) conference, organised by the e.V.

The current status report was delivered by Aleix Pol i Gonzàlez (chairperson of the board), Eike Hein (treasurer and deputy chairperson of the board), Lydia Pintscher (deputy chairperson of the board), Adriaan de Groot and Neofytos Kolokotronis.

The terms of office for Aleix, Lydia and Eike concluded with this General Assembly.

11 new active members have joined the e.V. since the last General Assembly.

The e.V has now 85 associate members as compared to 95 last year. Work on the infrastructure to retain associate members is ongoing. To this end, a new consultant was engaged.

One new company joined as associate member. One company terminated its associate membership, because it was acuqired and subsequently re-evaluated its position.

The Advisory Board was enlarged, consisting of representatives of KDE patrons and elected community partners.

The e.V. has one direct employee and 4 freelance contractors. Petra Gillert is a direct employee in the capacity of assistant to the Board, Aniqa Khokhar and Paul Brown work as marketing consultants, Adam Szopa as project coordinator, and Allyson Alexandrou as event coordinator.

The community report for 2019 concerning the activities of the community and the e.V. was presented.

Evolution of KDE: the next round of community goals is already ongoing. Additional structuring, documentation and support for the goalsetting and implementation processes within the e.V is taking place with the help of Adam Szopa.

Goals from the first round of goals continue to receive support.

Roles and responsibilities of the working groups of the e.V. have been systematically charted and clarified in collaboration with the working groups. The results have already been presented to the e.V. members and will be published after Akademy.

There were a few ideas to alter the statutes of the e.V and develop these further. The board is canvassing possible improvements. The process is still in an early phase and must be implemented carefully without any rush. The board asked for proposals.

KDE will celebrate its 25th birthday on 14 Oktober 2021. The board asked for ideas on how to use and celebrate the occasion.

#### Last year's goals of the board

* Organising Akademy 2020: the event took place. Due to the Corona situation, it was hosted as a virtual event. 600 participants have registered. The event is considered a success.
* Investing in the growth of our community members: more trainings have taken palce, such as implicit bias training. The extent of trainings must be increased further. The offer of access to professional libraries did not receive much interest. Ideas for future activities are welcome.
* Clarification of the role of KDE in environmental sustainability: travel has the most major direct impact on the environment. Becuase of the Corona situation, this has been greatly reduced. Another approach is being pursued via a project in the context of the Blauer Engel.
* Effectiveness of the board's network: clarification of roles and responsibilities of the working groups has concluded.
* Purposeful and effective spending of resources: in two areas, freelance consultants were hired and investments were made in the infrastructure for virtual events.
* Establishing processes: processes were improved, professionalised, and documented such as the process for onboarding new colleagues.
* Forging parternsips with other likeminded organisations: the Linux App Summit was successfully organised in collaboration with the GNOME project.

Two thirds of goals were achieved, other goals were obviated by changing circumstances.

#### Key themes for next year

* Coming together again: to the extent that health regulations will permit, in-person gatherings will held again. A call was made for hosts of Akademy 2021.
* Financial sustainability: infrastructure for associate members will be improved and the associate membership programme will be made more attractive.
* Sustainable employment: steps will be made to enable more people to make a living off of KDE related work.
* Supporting an open source friendly hardware ecosystem: the partnernetwork will be increased to advance open source friendly hardware.
* Imitating and strengthening what works: areas of KDE which are particularly successful will be identified, and lessons learned will be applied to other areas of KDE e.V.

### Report from the Tresurer

Eike Hein presents the Report from the Treasurer, which was written with the help of the Financial Working Group and the other members of the board.

#### Fiscal Year 2019

Income in 2019 was stable. There was no fundraising campaign, because of two large donations in the previous year, from the Pineapple Fund and Handshake Foundation.
Despite that, the overall income-situation is very stable. Eike emphasises that this is good for the association.

A few invoices for supporting membership in 2019 were paid in 2020.

Sponsoring for Akademy is developing well. The conference is financially stable.

Expenses were higher than income. This was planned, in order to satisfy the requirement to use income for the goals of the association
and to make use of the donations from previous here. Projected expenses were even higher, but money from the budget
for, e.g. the community-goals processes, was not requested or spent.

Events make up the largest part of regular spending for the association. The Linux App Summit is a new large event which
is co-funded and co-hosted by KDE e.V. The Randa meetings from previous years were displaced by the Linux App Summit.
A new Summit is planned.

Spending has continually grown in the past years.

Bookkeeping and the accounts were problematic this year, largely due to changes in personnel at the accountants office.
Steps are being taken to improve the situation.

Income is largely stable, especially in the corporate supporting memberships. Together, the supporting memberships provide most of the income.

Three-quarters of all expernses are for employees and contractors, and Akademy and other events.

#### Budget 2020

The budget-plan for 2020 was published late in june, because it needed modifications to account for the Corona-crisis.
The focus of the budget has moved from travel support to personnel costs. Additionally, the infrastructure
for virtual evens is being expanded.

The overall strategy of the budget, which is to increase the support for central areas of the community, has not changed.
Liquidity is very good. Financial reserves have increased. Cash reserves are now 650000 EUR, part of which is earmarked.

The community-goals team has its own earmarked funding and budget. The team has a clear mandate from the community for its spending.

#### Current State 2020

Income from donations is once again stable. One new corporate supporting member (Kontent GmbH) has joined
the association, while one Patron has left.

Akademy turns a profit, even though sponsorship levels were lowered for the virtual event.
The focus for Akademy is quality content for the community, not for realising a surplus.

Spending on contractors has increased markedly.

Spending on travel support is expected to rise sharply in future once travel becomes possible again.

The financial reserves held by the association allow for several years of current activity.
It is important to build up alternative financing for all activities.
The work done by the Fund Raising Working Group and other parts of the community supports that.
There are perhaps three years funding (with no income) available for building up
income sources and formulating sustainable plans. The association intends to
experiment and learn from various funding sources. The board indicates that
this will be done in the next time-frame.

### Bericht der Kassenprüfer

Die Kassenprüfer Andreas Cord-Landwehr und Ingo Klöcker berichten.

Die Mitglieder haben den Bericht der Kassenprüfer schon im Vorfeld per Email erhalten.

Die Kassenprüfer haben die Prüfung bei einem Treffen im KDE-Büro mit Lydia und Petra durchgeführt und haben Einsicht in die Bücher genommen.

Die Buchführung war von hoher Qualität, sogar noch höher als letztes Jahr. Es wurden nur bei sehr eingehender Betrachtung einige nicht wesentliche Feststellungen gemacht. Die Kassenprüfer sind mit der Buchführung sehr zufrieden.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2019.

### Entlastung des Vorstands

Der Versammlungsleiter erklärt die Bedeutung der Entlastung des Vorstands.

Der Versammlungsleiter bittet um Meldung zu Fragen und Bedenken gegenüber der Entlastung. Es gibt keine Meldungen.

Die Entlastung wird über eine Umfrage im Online-Tool BigBlueButton durchgeführt. Der Vorstand nimmt nicht an der Umfrage teil.

Das Ergebnis sind 77 Ja-Stimmen für die Entlastung, keine Gegenstimmen und 8 Enthaltungen.

Der Versammlungsleiter bittet um Meldung bei Bedenken gegenüber der Abstimmung. Es gibt keine Meldungen.

Damit ist der Vorstand entlastet.

### Weitere Berichte

Die Berichte der Arbeitsgruppen und Vertreter wurden auf der Akademy schon öffentlich vorgestellt und im Vorfeld per Email allen Mitgliedern zur Verfügung gestellt.

#### Bericht der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer und Martin Konold geben den Bericht als Vertreter des KDE e.V. in der KDE Free Qt Foundation.

Es finden weiterhin Verhandlungen mit The Qt Company über die Weiterentwicklung des Agreements zur Sicherstellung der Verfügbarkeit von Qt für freie Software-Entwicklung statt.

Ein Thema ist die teilweise Inkompatibilität der Bedingungen der kommerziellen Lizenz und der Verwendung als Open-Source. Die größten Probleme sind gelöst, aber es gibt immer noch ein paar Unstimmigkeiten.

Olaf bittet um ein Meinungsbild, ob das Thema weiter verfolgt werden soll. Bei 83 von 94 abgegebenen Meinungen stimmen 44 Mitglieder für die Weiterverfolung, 6 dagegen, und 33 enthalten sich.

In Kommentaren bringt die Mitgliedschaft zum Ausdruck, dass die Prioriät bei der Verfügbarkeit von Qt für die Entwicklung von freier Software liegen soll. Olaf erklärt, dass dies in jedem Fall gegeben ist, da es der satzungesgemäße Zweck der KDE Free Qt Foundation ist.

Olaf und Martin bringen den Vorschlag auf, eine Arbeitsgruppe zu bilden, um die Arbeit der Vertreter in der KDE Free Qt Foundation zu unterstützen.

#### Andere Gruppen

Die Financial Working Group besteht aus Mitgliedern die für die Dauer von zwei Jahren gewählt wurden. Diese Dauer ist für einige Mitglieder erreicht. Die Financial Working Group ruft auf, sich als neue Mitglieder der Arbeitsgruppe zu bewerben.

Die Advisory Board Working Group hat zum Zweck, die Beziehung zwischen KDE und seinen Partnern zu pflegen, diesen Ansprechpartner zu bieten. Sie hat dieses Jahr eine Telefon-Konferenz durchgeführt, um die Mitglieder des Advisory-Boards über wichtige Entwicklungen in KDE zu informieren. OpenUK wurde als neues Mitglied des Advisory-Boards aufgenommen.

Die Community Working Group hat ein relativ hohes Aufkommen an Fragen von Menschen, die sich der Community anschließen wollen. Ihr eigentlicher Zweck ist allerdings, bei der Lösung von Konflikten in der Community zu helfen. Es wird angeregt, darüber nachzudenken, den Namen der Arbeitsgruppe anzupassen, um ihre Aufgabe genauer zu reflektieren.

### Wahlen

Es stehen drei Wahlen auf der Tagesordnung. Die Wahlen werden online mit Hilfe des Online-Services [Belenios](https://www.belenios.org) durchgeführt, der sichere, geheime, überprüfbare Wahlen ermöglicht.

Jeff Mitchell, der die technische Betreuung des Wahldienstes übernommen hat, erläutert den Wahlprozess. Die drei Wahlen werden in einem gemeinsamen Vorgang durchgeführt. Nach der Benennung der Kandidaten wird die Kandidaten-Liste geschlossen und es werden per Email die Zugangsdaten zum Wahldienst an alle zu diesem Zeitpunkt anwesenden und stimmberechtigten Mitglieder verschickt. Damit können die Mitglieder ihre Stimmen abgeben und anhand der beigefügten Informationen auch sicher nachvollziehen, dass ihre Stimmen im Wahlergebnis berücksichtigt wurden.

#### Kandidaten

Im Vorstand gibt es drei offene Positionen, da die Amtszeit der entsprechenden Vorstandsmitglieder abgelaufen ist. Alle drei Vorstandsmitglieder stellen sich zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl in den Vorstand:

* Aleix Pol Gonzalez
* Eike Hein
* Lydia Pintscher

Als Kassenprüfer stellen sich die Kassenprüfer des letzen Jahres zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl zum Kassenprüfer:

* Andreas Cord-Landwehr
* Ingo Klöcker

Für die Vertreter des KDE e.V. in der KDE Free Qt Foundation stellen sich die bisherigen Vertreter zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl zum Verteter des KDE e.V. in der KDE Free Qt Foundation:

* Martin Konold
* Olaf Schmidt-Wischhöfer

#### Voting Results

There are 89 voters registered electronically.

Results of board election:

* Aleix Pol Gonzalez: 89 Votes
* Eike Hein: 88 Votes
* Lydia Pintscher: 86 Votes

All three candidates have been re-elected. All three candidates accept the election.

Results of the election of the auditors of accounting:

* Andreas Cord-Landwehr: 88 Votes
* Ingo Klöcker: 86 Votes

Both candidates have been re-elected. Both candidates accept the election.

Results of the election of KDE e.V.representatives to the KDE Free Qt Foundation:

* Martin Konold: 89 Votes
* Olaf Schmidt-Wischhöfer: 84 Votes

Both candidates have been re-elected. Both candidates accept the election.

An error in determining the list of valid voters has prevented four of the members of the association, who are present, from obtaining an electronic voting registration.
All other members who are present have voted.
The four members who are affected each state that they do not object to the voting result.
No replacement vote is needed and the elections are declared valid.

### Miscellaneous

There is a discussion about progress in the context of the KDE Free Qt Foundation.

A request for a straw poll for a Working Group to support the KDE Free Qt Foundation shows:
45 for, 9 against, no abstentions.

### Closing

The general assembly is closed by the chairperson at 17:30.

The general assembly was not interrupted by sound- or video-problems.

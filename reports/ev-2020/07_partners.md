### [Advisory Board](https://ev.kde.org/advisoryboard.html)</a>

The [KDE Advisory Board](https://ev.kde.org/advisoryboard.php) is a group of representatives of KDE e.V.'s patrons and other select organizations that are close to KDE's mission and community. It currently has 13 members, and there is a KDE e.V. working group with community members acting as direct contacts for our partners.

The Advisory Board members held two calls in 2019, one in March and one in July, with the goal always being to inform the members of the board, receive feedback and discuss topics of common interest.

One of the main topics of discussions that took place in the calls revolved around KDE's active goals and the second round of goal proposals. In addition, the members were briefed on KDE's Akademy 2019 status, our flagship community event, as well on the Linux App Summit 2019, co-organized by KDE and GNOME. It is worth noting that during Akademy's 2019, the members were also invited to participate in Akademy's sponsor's dinner, as it was a great opportunity to catch up and bond in a more relaxed manner. The members were also informed on all the development sprints that took place since the last call, and were given a chance to raise topics of their interest.

The Advisory Board is a place and a symbol of KDE's collaboration with other organizations and communities firmly standing behind the ideals of Free and Open Source Software.

**Current members:** Blue Systems, Canonical, City of Munich, Debian, enioka Haute Couture, FOSS Nigeria, FSF, FSFE, OpenUK, OSI, SUSE, The Document Foundation, The Qt Company.

### [Patrons](https://ev.kde.org/supporting-members.html)

**Current patrons:** Blue Systems, Canonical, enioka Haute Couture, Google, The Qt Company, Slimbook, SUSE.

**Current supporters:** KDAB, basysKom, Kontent

### [Community Partners](https://ev.kde.org/community-partners.html)

**Current community partners:** Qt Project, Lyx and Randa Meetings.

<table width="100%"> <tr> <td style="vertical-align: top;"> <table class="table"> <thead> <tr> <th><h3>Income (€):</h3></th> <th></th> </tr> </thead> <tbody>

```
       <tr>
            <td>Patrons:</td>
                <td class="text-right">43,900.00</td>
            </tr>
            <tr>
                <td>Supporting members & donations (sans e7: Retour):</td>
                <td class="text-right">140,791.44</td>
            </tr>
            <tr>
                <td>Akademy</td>
                <td class="text-right">24,430,00</td>
            </tr>
            <tr>
                <td>Other Events</td>
                <td class="text-right">0.00</td>
            </tr>
            <tr>
                <td>GSoC and Code in</td>
                <td class="text-right">9,487.09</td>
            </tr>
                            
            <tr>
                <td>Other</td>
                <td class="text-right">215.34</td>
            </tr>                
            <tr>
                <td><b>Total Income:</b></td>
                <td class="text-right"><b>218,823.87</b></td>
            </tr>
            </tbody>
        </table>
    </td>
    
    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    
    <td style="vertical-align: top;">
        <table class="table">
            <thead>
                <tr>
                <th><h3>Expenses (€):</h3></th>
                <th></th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>Sprints and meetings (sans Akademy):</td>
                <td class="text-right" style="color: red">-6,304.04</td>
            </tr>
            <tr>
                <td>Other Events:</td>
                <td class="text-right" style="color: red">-9,737.30</td>
            </tr>
            <tr>
                <td>Akademy:</td>
                <td class="text-right" style="color: red">-6,847.40</td>
            </tr>
            <tr>
                <td>Personnel costs:</td>
                <td class="text-right" style="color: red">-112,206.47 </td>
            </tr>
            <tr>
                <td>Infrastructure:</td>
                <td class="text-right" style="color: red">-5,810.05</td>
            </tr>
            <tr>
                <td>Office:</td>
                <td class="text-right" style="color: red">-8,791.35</td>
            </tr>
            <tr>
                <td>Taxes and Insurance:</td>
                <td class="text-right" style="color: red">-22,183.03</td>
            </tr>
            <tr>
                <td>Other:</td>
                <td class="text-right" style="color: red">-7,370.41</td>
            </tr>
            <tr>
                <td><b>Total:</b></td>
                <td class="text-right" style="color: red"><b>-179,250.05</b></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
```

</table>

<table> <tr> <td> <img src="images/FIWG/FIWG_2020_split.png"/></td> </tr> </table>

### Report

In the early months of 2020, the emerging Covid-19 pandemic presented us with difficult to predict consequences for the ecosystem around our organization. We decided to revise our ongoing budget planning mid-year based on new information and also adopt a fiscally conservative approach that would allow us to adjust spending with shorter lead times if needed.

The pandemic's largest impact by far has been on one of KDE e.V.'s major activities, the organization and financing of events and event attendance. The community's annual flagship event, the user and developer conference Akademy, decided to adopt an online-only format, as did development sprint meet-ups and other events around the broader Free Software community we commonly attend.

Joining in the effort, we reallocated the travel subsidy budget to installing a re-usable infrastructure (servers and software for video conferencing among other things) to run online events. This infrastructure has since seen regular use and we consider it a significant addition to the community's operational tool set.

In addition to infrastructure, we also created and contracted for the new position of Event Coordinator to help us further improve the quality of our events. Originally meant to help the Akademy team conduct an improved in-person Akademy, this new capability allowed us to adapt to the changing situation with speed. Initially, in the form of a short-term work package, we have since decided to maintain the position longer-term to help us further improve our events also in 2021. It will be the second year the community will rely on the online-only format, and we aim for continuity to apply the experience we gained in 2020.

KDE e.V.'s major sources of income - donations from private individuals and corporations - generally remained stable or increased throughout 2020. In particular, donations from individual donors increased significantly. A highlight was the large one-time donation [from the Handshake Foundation](https://dot.kde.org/2020/01/21/kde-receives-generous-donation-handshake-foundation) we received in January. Other individual donations increased by over 40%. In an economically challenging and unpredictable year, we more than ever do not take the commitment of our donors to the KDE community for granted. We would like to once again thank them warmly for their generosity.

Significantly reduced in 2020 vs. the preceding year was our income from two mentorship programs, Google Summer of Code and Google Code-in. This was in line with our expectations and financial planning. Further, a higher than usual amount of 23.500 EUR of corporate membership fees invoiced in 2020 was received at a delay in 2021. This income will be shown for the year 2021 in the following Annual Report, and this fact is important to bear in mind while studying figures for 2020. Also delayed until 2021 was the payment of income from Linux App Summit (LAS) 2020 sponsorships, due to the settlement process between the parties co-organizing the event. This amounts to an additional 2.724 EUR.

In summary, KDE e.V.'s income in 2020 exceeded its expenditure and our financial assets have grown. The great reduction in our spending on travel subsidy has allowed us to focus on expanding our engagement of professional contractors and the execution of projects, but we did so with the caution an unpredictable situation demanded and will continue to demand.

We think this sets us up well for 2021, in which we will continue to carefully monitor the effects the Covid-19 pandemic has on our community and the industry. With the re-usable infrastructure for online events now in place and travel unlikely to be a significant spending activity until late 2021 at the earliest, we will focus spending on several new contracted positions aimed at, for example, helping the community to improve technical documentation for developers and coordinate an expanding ecosystem of device manufacturers adopting KDE technology.

**Financial Support:** If you or your company are interested in financially supporting the KDE Community on an ongoing basis, please visit the [Supporting Members page](https://ev.kde.org/getinvolved/supporting-members/) on the KDE e.V. website.
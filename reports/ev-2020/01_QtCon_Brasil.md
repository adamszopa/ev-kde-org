<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/QtConBR/qtconbr.png"><img src="images/QtConBR/qtconbr.png" width="100%" /></a></figure>
</div>


The fourth edition of [QtCon Brasil](https://br.qtcon.org/) was held online from 26th to 27th September 2020. QtCon Brasil is the only conference in Brazil and Latin America entirely dedicated to the Qt libraries, utilities and frameworks. Since its first edition, held in 2017 in the city of São Paulo, QtCon Brasil has acted as an important forum for users, universities, government institutions, companies and free software communities to share their experiences, business knowledge and learn about the latest in UI technologies.

This year, KDE sponsored QtCon Brasil as usual, and many speakers from KDE participated, including Aleix Pol, Camilo Higuita, Sandro Andrade, and Patrick Pereira.

The conference was opened by Aracele Torres from [Qmob Solutions](https://qmob.solutions/) [opened the conference](https://www.youtube.com/watch?v=Sg2dJmY8TJA) along with Jose Valecillos from [The Qt Company](https://qmob.solutions/) USA. The keynotes were followed by talks on a wide variety of topics, including [Qt and the Crystal Ball](https://www.youtube.com/watch?v=TX_MoKxQRw4) by Corey Pendleton, [UIs for humans](https://www.youtube.com/watch?v=pbimTM0MZw8) by Nuno Pinheiro, [Delivering with Wayland](https://www.youtube.com/watch?v=s2TU38Ai7_4) by Aleix Pol, [Convergence with Qt](https://www.youtube.com/watch?v=l7Rrx74W1fA) by Camilo Higuita, [An Introduction to Qt for Python](https://www.youtube.com/watch?v=QsVqnc6_SgQ) by Mariana Meireles, [Qt Containers and Algorithms](https://www.youtube.com/watch?v=8BRtxOx7Z50) by Thiago Macieira, [Rapid Prototyping with QML](https://www.youtube.com/watch?v=PgmrFTV9ric) by Patrick Pereira, [Qt 6 - What's Next?](https://www.youtube.com/watch?v=W0eY2C1vM9c) by Sandro Andrade, and many more.

QtCon Brazil aims to be a forum where people interested in Qt can share their experiences, know a bit more about successful use-cases like KDE, and learn how Qt can be used in business.

### HaQton

QtCon Brasil is usually followed by a Qt programming competition called "[The HaQton](<https://br.qtcon.org/regulamento-haqton-2020.pdf>)". The HaQton aims to promote the use of Qt in the Brazilian and Latin American communities and the registered teams develop a Qt application that requires the use of functionalities related to UIs, multimedia, network communication via RESTful APIs and message bus communications.

One of the aims of the Haqton is to give organizers the opportunity to discover new talents that can join the Brazilian and Latin American Qt ecosystem. Past HaQton participants have gone on to become speakers in later editions of QtCon Brazil, contributors to projects such as KDE, etc.

[This year's winners](https://br.qtcon.org/haqton-vencedores.pdf) were also awarded cash prizes and development boards for embedded projects.

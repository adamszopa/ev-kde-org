KDE's participation in [FOSDEM 2020](https://archive.fosdem.org/2020/) went pretty well, without a great deal of coordination overhead.

### Talks

KDE community members had three talks at FOSDEM 2020:

* Akhil Gangadharan Kurungadathil delivered a talk called [Rendering QML to make videos in Kdenlive](https://archive.fosdem.org/2020/schedule/event/om_qml/), in which he explained how QML, a language prominently used for designing UIs, could be used to create title video clips containing text and/or images. These titles and images can then be rendered and composited over videos in the video editing process. Akhil worked on this for Kdenlive's Google Summer of Code 2019 project ([Kdenlive](https://kdenlive.org) is KDE's full-featured video editor) and the project is still under active development.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/FOSDEM/akhil.jpg"><img src="images/FOSDEM/akhil.jpg" width="100%" /></a><br /><figcaption>Akhil explains how to us QML to create assets for Kdenlive projects.</figcaption></figure>
</div>


* Adriaan de Groot, developer of Calamares and KDE e.V. Board member, talked about [the state of the Plasma desktop and applications on FreeBSD](https://archive.fosdem.org/2020/schedule/event/kde_on_freebsd/), reminding the world once again that KDE's software works more or less everywhere.
* Finally, Volker Krause talked about the [Itinerary project](https://archive.fosdem.org/2020/schedule/event/kde_itinerary/), KDE's fledgeling travel assistant. He discussed the privacy concerns in current proprietary alternatives, and how [Itinerary](https://apps.kde.org/en/itinerary) was planning to address these concerns, without sacrificing functionalities.

### Booth

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/FOSDEM/booth.jpg"><img src="images/FOSDEM/booth.jpg" width="100%" /></a><br /><figcaption>KDE's booth at FOSDEM 2020.</figcaption></figure>
</div>


This year, instead of being located at the back entrance for the desktops, we were right at the front entrance, where all the people came in. This meant a constant stream of people, but also that it was the most crowded spot, which made it hard to stand still in front of the booth. The booth itself was decked out with a KDE-themed tablecloth, a rollup and a 27" monitor which ran a slideshow showcasing KDE stuff to visitors.

We also had an assortment of electronic KDE-enabled devices visitors could play with. Among other things, we had some mobile [devices running Plasma Mobile](https://www.pine64.org/pinephone/), a Pinebook Pro, and some laptops to showcase live KDE's [Plasma desktop](https://kde.org/plasma-desktop/) and [applications](https://apps.kde.org/).

In the merch department, we had a selection of stickers from various KDE sources and a bunch from many other projects (more about this later). We did not have beer mats this year, which was a pity, since they were popular in prior editions; but there were T-shirts, although we were not selling stuff and didn't take monetary donations at the stand this year. People asking for T-shirts were re-directed to freewear.org.

I think that if we had had staff coming by car, then getting T-shirts from freewear under our agreement with them (sent first to that person's home, then brought by car) would have been the way to go.

We did not have a booth roster, but that worked out just fine with people just showing up and helping out. We did a lot of the cross-promotion with openSUSE, Mandriva, FreeBSD, research projects, and others: they sent people to us, and we sent people to them. It was a win-win all around, so let's keep doing that!

Kenny made some great name tags we used for the duration of the event. There were always people at the booth, generally, two in front of the table and one or two behind, and our talking points hinged on finding out what visitors knew about KDE and demonstrating how KDE software runs everywhere. Often it was Dave Edmundson out front asking visitors: "Do you know what KDE does?" and then inviting them to try it. Meanwhile, Akhil efficiently staffed the booth.

Getting direct feedback from users on the UI of our software was a great help and it was interesting to note that quite a few people asked us about Qt licensing. For future events, it would be good to make more slideshows we can run on the monitor, just for variety's sake. Also, the one we showed was a plain QML slideshow, but with a Kdenlive-talented person, we could make a much better one.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/FOSDEM/linuxphone.jpg"><img src="images/FOSDEM/linuxphone.jpg" width="100%" /></a><br /><figcaption>Members from Pine64, Manjaro, UBports and Plasma Mobile meet after hours to enjoy a meal and talk about the future of free mobile phones.</figcaption></figure>
</div>

### Effectiveness

The Promo team had suggested some metrics we could use to measure the effectiveness of the booth, but we didn't apply any of them ☹. We didn't have anyone with a good camera, so we did not get much in the way of promo pictures either.

That said, FOSDEM is a weird kind of event, as it is one of those places where "you have to be". The value is not exclusively in the booth, but having people there who talk to other people and coordinating KDE activities with the rest of the world. I planned two more KDE events for later in the year while there (although we all know how that ended up working out) and coordinated some development work with GNOME. We synced up FreeBSD X11's Wayland needs, managed to show off Kdenlive stuff to visitors, and Plasma Mobile gained several more contributors. None of this was directly due to the booth, but because we had warm bodies and warm smiles for everybody that visited us in Brussels.

For the second year in a row, the [Plasma Mobile](https://www.plasma-mobile.org/) team met in [KDAB’s](https://www.kdab.com/) offices in Berlin from the 3rd to the 9th of February to participate in a Plasma Mobile sprint.

<div> <video width="99%" autoplay loop muted> <source src="images/PlasmaMobile/VID_20200214_140132.mp4" type="video/mp4"> </video> </div>

We covered many areas:

### Shell and Design

Marco Martin re-worked the shell user interface and [his blog](https://notmart.org/blog/2020/02/fosdem-and-plasma-mobile-sprint/) contains all the details of his progress.

Thanks to Mathis Brüchert, many Plasma Mobile apps got new icons. Mathis also worked on the Plasma Mobile website. The *Find your way* page’s appearance was updated to be consistent with the rest of the website. Jonah worked on a mobile-friendly open/save file dialog and Linus added quick access to known places to the new file dialog -- this is similar to what you can find in [Dolphin](https://apps.kde.org/en/dolphin), KDE Plasma's desktop file manager. Linus also worked on adding a screenshot action to the top drawer. Marco Martin fixed an issue in [kdeclarative](https://api.kde.org/frameworks-api/frameworks-apidocs/frameworks/kdeclarative/html/index.html) that was causing the WiFi settings to show an empty message box.

### KWin/Wayland

Aleix Pol implemented the [zwp_input_method_context_v1_ protocol](https://smithay.github.io/wayland-rs/wayland_protocols/unstable/input_method/v1/server/zwp_input_method_context_v1/index.html) in [KWin/Wayland](https://community.kde.org/KWin/Wayland), which allowed us to make use of the [maliit_ keyboard](https://maliit.github.io/), [ibus](https://github.com/ibus/ibus/wiki) and other input methods in Plasma. Aleix also implemented [EGL_KHR_partial_update](https://docs.imgtec.com/Supported_Extensions_OpenGL_ES_EGL/topics/d0e1060.html) which will improve the performance on ARM GPUs that support tiled rendering. Tobias Fella fixed the bug that had the virtual keyboard stay open when the screen was locked.

### Applications

Jonah implemented audio visualization in [Voicememo](https://invent.kde.org/cahfofpai/voicememo), Plasma Mobile’s audio recorder application. He also reviewed a patch by Rinigus Saar that rewrites the tab switcher of [Angelfish](https://invent.kde.org/plasma-mobile/angelfish), Plasma Mobile’s browser application. Tobias started work on an RSS feed reader and our camera application now has a clear indication of when the properties are checkable. Nicolas Fella worked on cleaning up the dialer’s codebase.

KDE contributor cahfofpai worked on improving the list of mobile GNU/Linux applications. The list now includes new applications like [KTrip](https://apps.kde.org/en/ktrip), [Kamoso](https://apps.kde.org/en/kamoso), voicememo, [Ruqola](https://apps.kde.org/en/ruqola), [Kongress](https://apps.kde.org/en/kongress), and [Keysmith](https://apps.kde.org/en/keysmith), and was cleaned up by removing some duplicate applications. Workflow for editing the list was also improved and, along with Bhushan Shah, they reviewed the list of currently pre-installed applications in Plasma Mobile.

cahfofpai also worked with Jonah Brüchert on testing various mobile-friendly GNOME applications included in the [KDE Neon](https://neon.kde.org/) image. Both developers looked into what needed improvement and tested Flatpak support on the [PinePhone's](https://www.pine64.org/2020/12/01/kde-community-edition-is-now-available/) KDE Neon image. They found that the binary factory was not creating arm64 Flatpak builds.

Furthermore, working on [Kaidan](https://www.kaidan.im/), a Jabber/XMPP chat client for Plasma and Plasma Mobile, cahfofpai figured out a bug in its Flatpak recipe which prevented the app from detecting the camera. He went on to investigate why [KDE Itinerary](https://apps.kde.org/en/itinerary) fails to import files with special characters in their names.

### Kirigami

Camilo Higuita and Marco Martin worked on expanding some of [Kirigami](https://develop.kde.org/frameworks/kirigami//)'s features inspired by [Maui](https://mauikit.org/) apps. These included pull back headers and footers, which is a common pattern on Android and that help to focus the app’s main content on small screens. They also fixed some issues in Kirigami’s *ActionToolBar* which is used in Maui’s *SelectionBar*.

Work on Maui apps allowed lasso selection when a mouse or keyboard is present, improving the support for desktop systems, while still keeping them touch-friendly.

### Android

There's an ongoing effort to make our mobile apps available on Android to grow the target audience and allow people to work on/test our apps even if they don't have a Linux phone. Thanks to Nicolas Fella work during the sprint, it became possible for users to get Kaidan, [qrca](https://invent.kde.org/plasma-mobile/qrca), Keysmith, and Kongress for Android from KDE’s binary factory and [F-Droid](https://f-droid.org/)'s repository. Nicolas also fixed an issue with [KNotifications](https://invent.kde.org/frameworks/knotifications) on Android that affected Kaidan.

Then, Volker Krause, Nicolas Fella and Aleix Pol discussed upgrading our Android builds to Qt 5.14, which promised various improvements.

### Collaborations

This year we were joined by two [UBports](https://ubports.com/) developers, Marius Gripsgard and Dalton Durst. The main goal was to foster collaboration between the KDE and UBports communities. We discussed sharing software components for common needs. This includes solutions for content sharing, telephony, permission management, push notifications and configuration management.

We also discussed how to get KDE applications working on Ubuntu Touch and vice versa. Jonah worked on the Ubuntu Touch Flatpak runtime to enable running the Ubuntu Touch application in various mobile distributions. Nicolas convinced Dalton and Marius to upgrade the Qt version shipped with Ubuntu Touch, allowing KDE apps to work there. We also created a proof-of-concept Click package (Ubuntu Touch's package format) for KTrip.

On Saturday, [we joined the UBports Q&A](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-q-a-69-3706), a bi-weekly show hosted by the UBports team, where we discussed the work done during the sprint.
[GNUHealthCon 2020](https://www.gnuhealthcon.org/2020-online/) was held online from the 20th to 21th of November.

It was not easy... We’re so used to celebrating the GNU Health Conference (GHCon) and the International Workshop on eHealth in Emerging Economies (IWEEE) in a physical location, that changing to a virtual conference was challenging. At the end of the day, we are about Social Medicine, and social interaction is a key part of it.

The pandemic has changed many things, including the way we interact. So, we decided to work on a Big Blue Button instance, and switch to virtual hugs for this year. Surprisingly, it worked out very well. We had colleagues from Gabon, Brazil, Japan, Austria, United States, Argentina, Spain, Germany, Chile, Belgium, Jamaica, England, Greece and Switzerland. We didn’t have any serious issues with the connectivity, and all the live presentations went fine. The time zone difference among countries was a bit challenging, especially to our friends from Asia, but they made it!

### Qt and KDE projects in the spotlight

If we think about innovation in computing, we think about Qt and KDE. GNU Health integrates this bleeding edge technology in [MyGNUHealth](https://invent.kde.org/pim/mygnuhealth/), the GNU Health Personal Health Record for desktop and mobile devices that uses Qt and [Kirigami frameworks](https://develop.kde.org/frameworks/kirigami//).

Aleix Pol, president of KDE e.V., presented [Delivering Software like KDE](https://www.gnuhealthcon.org/2020-online/presentations/aleix_pol_GHCon2020.pdf), emphasizing delivering code that would be valid for many different platforms, especially mobile devices.

Cristián Maureira-Fredes, leader of the Qt for Python project, in his presentation [Qt for Python: past, present, and future!](https://www.gnuhealthcon.org/2020-online/presentations/cristian_maureira_GHCon2020.pdf), talked about the history and the upcoming developments in the project, such as PySide6, the latest Python package and development environment for Qt6. MyGNUHealth is a PySide application, so we’re very happy to have Cristián and Aleix on the team!

Dimitris Kardarakos presented a key concept in modern applications: convergence, that is, the property of an application to adapt to different platforms and geometries. His talk, [Desktop/mobile convergent applications with Kirigami](https://www.gnuhealthcon.org/2020-online/presentations/dkardarakos_GHCon2020.pdf), explained how this framework works on KDE, the implementation of KDE's Human Interface Guidelines, and how it helps the developers create convergent, consistent applications from the same codebase. MyGNUHealth is an example of a convergent application, as it can be used both on the desktop and as a mobile device app.

As for me, I went into details on MyGNUHealth design in my talk, [MyGNUHealth: The GNU Health Personal Health Record](https://www.gnuhealthcon.org/2020-online/presentations/MyGNUHealth%20-%20GHCon2020.pdf).